#include "UpcastingEmployee.h"
#include <iostream>
using namespace std;
UpcastingEmployee::UpcastingEmployee(string name, string surname, float age, double salary)
{
	cout << "Upcasting Employee Constructor Worked!" << endl;
	mName = name;
	mSurname = surname;
	mAge = age;
	mSalary = salary;
	//displayInfo();
}

UpcastingEmployee::~UpcastingEmployee()
{
	cout << "Upcasting Employee Destructor Worked!" << endl;
}

void UpcastingEmployee::displayInfo()
{
	cout << "EMPLOYEE INFO!!!!" << endl;
	cout << "Name :  " << mName << endl;
	cout << "Surname :  " << mSurname << endl;
	cout << "Age :  " << mAge << endl;
	cout << "Salary :  " << mSalary << endl;
}
void UpcastingEmployee::printRefereence(UpcastingEmployee& upcastingEmployee)
{
	cout << "EMPLOYEE REFERENCE!!!!" << endl;
	upcastingEmployee.displayInfo();
}
void UpcastingEmployee::printPointer(UpcastingEmployee* upcastingEmployee)
{
	cout << "EMPLOYEE POINTER!!!!" << endl;
	upcastingEmployee->displayInfo();
}
void UpcastingEmployee::printMyType(UpcastingEmployee upcastingEmployee)
{
	cout << "EMPLOYEE TYPE!!!!" << endl;
	upcastingEmployee.displayInfo();
}

