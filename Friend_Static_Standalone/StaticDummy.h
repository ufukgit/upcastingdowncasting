#pragma once
#include <iostream>

class StaticDummy
{
public:
	StaticDummy();
	~StaticDummy();
	static float doAddition(float x, float y);

private:
	int hours = 0;
	int minutes = 0;
	int seconds = 0;
	void displayTime();
	void myTimer();

};

