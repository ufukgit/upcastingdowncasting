#include "StaticDummy.h"
#include <iostream>
#include <iomanip>
#include <thread>
#include <stdlib.h>
#include <chrono>
#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif
using namespace std;
StaticDummy::StaticDummy()
{
	cout << "StaticDummy class constructor worked\n";
	myTimer();
}

StaticDummy::~StaticDummy()
{
	cout << "StaticDummy class destructor worked\n";
}

float StaticDummy::doAddition(float x, float y)
{
	cout << "Addition Result: " << x + y << endl;
	return x + y;
}

void StaticDummy::displayTime()
{
	system("cls");

	cout << "         TIMER         \n";
	cout << "| " << setfill('0') << setw(2) << hours << " hrs | ";
	cout << setfill('0') << setw(2) << minutes << " min | ";
	cout << setfill('0') << setw(2) << seconds << " sec |" << endl;
}

void StaticDummy::myTimer()
{
	//while (1)
	//{
	//	displayTime();
	//	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	//	seconds++;
	//	if (seconds == 60)
	//	{
	//		minutes++;
	//		if (minutes == 60)
	//		{
	//			hours++;
	//			minutes = 0;
	//		}
	//		seconds = 0;
	//	}
	//}
}
