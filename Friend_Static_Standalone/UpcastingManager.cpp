#include "UpcastingManager.h"
#include <iostream>
using namespace std;
UpcastingManager::UpcastingManager(string name, string surname, float age, double salary, string mngdepartment) 
	: UpcastingEmployee(name, surname, age, salary)
{
	cout << "Upcasting Manager Constructor Worked!" << endl;
	mManageDepartment = mngdepartment;
	//displayInfoManager();
}

UpcastingManager::~UpcastingManager()
{
	cout << "Upcasting Manager Destructor Worked!" << endl;
}

void UpcastingManager::displayInfoManager()
{
	cout << "MANAGER INFO!!!!" << endl;
	UpcastingEmployee::displayInfo();
	cout << "Department  :" << mManageDepartment << endl;
}

void UpcastingManager::printRefereence(UpcastingManager& upcastingManager)
{
	cout << "MANAGER TYPE!!!!" << endl;
	upcastingManager.displayInfoManager();
}

void UpcastingManager::printPointer(UpcastingManager* upcastingManager)
{
	cout << "MANAGER TYPE!!!!" << endl;
	upcastingManager->displayInfoManager();
}

void UpcastingManager::printMyType(UpcastingManager upcastingManager)
{
	cout << "MANAGER TYPE!!!!" << endl;
	upcastingManager.displayInfoManager();
}
