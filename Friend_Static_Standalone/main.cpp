#include <iostream>
#include "StaticDummy.h"
#include "StaticExamples.h"
#include "UpcastingEmployee.h"
#include "UpcastingManager.h"
int main()
{
    //static examples;

    //StaticDummy StaticDummy;
    //StaticExamples StaticExamples;
    //cout << endl;
    //cout << endl;

    //upcasting examples

    //UpcastingEmployee* emp1;
    //UpcastingManager manager("Cenk", "Sahin", 36, 50000, "Director");
    //emp1 = &manager;
    //cout << endl;
    //cout << endl;

    //downcasting examples;

    //UpcastingManager* manager2 = (UpcastingManager*)(emp1);
    //emp1->displayInfo();
    //manager2->displayInfoManager();
    //cout << endl;
    //cout << endl;
    //cout << endl;
    //cout << endl;

    // By Pass Value & Reference Example

    UpcastingEmployee employee1("Ufuk", "Mentes", 27, 25000);
    UpcastingManager manage1("Cenk", "Sahin", 41, 56000, "manager");

    employee1.printRefereence(employee1);
    manage1.printRefereence(manage1);


}