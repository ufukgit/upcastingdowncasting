#pragma once
#include <iostream>
using namespace std;
class UpcastingEmployee
{
public:
	string mName, mSurname;
	float mAge;
	double mSalary;
	UpcastingEmployee(string name, string surname, float age, double salary);
	~UpcastingEmployee();
	void displayInfo();

	void printRefereence(UpcastingEmployee&);
	void printPointer(UpcastingEmployee*);
	void printMyType(UpcastingEmployee);
};

