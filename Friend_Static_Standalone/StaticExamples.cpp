#include "StaticExamples.h"
#include "StaticDummy.h"
#include <iostream>
#include <ctime>
using namespace std;

StaticExamples::StaticExamples()
{
	myTransaction();

	staticFunction1();
	staticFunction1();
	staticFunction1();

	cout << endl;

	staticFunction2();
	staticFunction2();
	staticFunction2();

	cout << endl;

	staticFunction3();
	staticFunction3();
	staticFunction3();

	cout << endl;

	staticFunction4();
	staticFunction4();
	staticFunction4();

	cout << endl;

}

StaticExamples::~StaticExamples()
{
}

void StaticExamples::myTransaction()
{
	srand(time(NULL));
	StaticDummy::doAddition(rand()%1500, rand()%250);
	cout << "Static Function Worked!\n";
}

void StaticExamples::staticFunction1()
{
	int sayac = 0;
	sayac++;
	cout << "static function 1 worked  :" << sayac << endl;
}

void StaticExamples::staticFunction2()
{
	static int sayac = 0;
	sayac++;
	cout << "static function 2 worked  :" << sayac << endl;
}

void StaticExamples::staticFunction3()
{
	static int sayac = 0;
	sayac++;
	cout << "static function 3 worked  :" << sayac << endl;
}

void StaticExamples::staticFunction4()
{
	static int sayac = 7;
	sayac++;
	cout << "static function 4 worked  :" << sayac << endl;
}