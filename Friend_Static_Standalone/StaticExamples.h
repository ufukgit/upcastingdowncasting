#pragma once
#include <iostream>

class StaticExamples
{
public:
	StaticExamples();
	~StaticExamples();
private:
	void myTransaction();
	void staticFunction1();
	void staticFunction2();
	void staticFunction3();
	void staticFunction4();

};

