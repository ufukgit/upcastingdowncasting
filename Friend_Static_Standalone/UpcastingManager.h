#pragma once
#include <iostream>
#include "UpcastingEmployee.h"
using namespace std;
class UpcastingManager : public UpcastingEmployee
{
public:
	string mManageDepartment;
	UpcastingManager(string name, string surname, float age, double salary, string mngdepartment);
	~UpcastingManager();
	void displayInfoManager();

	void printRefereence(UpcastingManager&);
	void printPointer(UpcastingManager*);
	void printMyType(UpcastingManager);
};

